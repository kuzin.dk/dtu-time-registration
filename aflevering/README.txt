Eclipse exported the zip file successfully, but if there are any issues with it, 
please clone the repository at

 - https://gitlab.com/Daniel_Kuzin/dtu-time-registration 
 - https://gitlab.gbar.dtu.dk/s184206/dtu-time-registration/

and use the Import from File System function in Eclipse.