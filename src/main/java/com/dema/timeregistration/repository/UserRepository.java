package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User> {
    Optional<User> findByEmail(String email);

    boolean existsByEmail(String email);

    List<User> findAll();

    long countByGlobalRole(GlobalRole globalRole);
}
