package com.dema.timeregistration.repository.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Duration;

/**
 * Converts a Java 8 Duration to a Long with minute precision for database storage
 */
@SuppressWarnings("unused")
@Converter(autoApply = true)
public class DurationAttributeConverter implements AttributeConverter<Duration, Long> {
    @Override
    public Long convertToDatabaseColumn(Duration duration) {
        return duration.toMinutes();
    }

    @Override
    public Duration convertToEntityAttribute(Long mins) {
        return Duration.ofMinutes(mins);
    }
}
