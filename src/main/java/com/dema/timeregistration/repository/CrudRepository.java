package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.EntityWithId;

import java.util.Optional;

public interface CrudRepository<TEntity extends EntityWithId> {
    void save(TEntity entity);

    Optional<TEntity> findById(int id);

    void delete(TEntity entity);
}
