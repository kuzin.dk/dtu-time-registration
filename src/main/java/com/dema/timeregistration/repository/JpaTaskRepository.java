package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.entity.Task;
import com.dema.timeregistration.repository.jpa.AbstractJpaRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class JpaTaskRepository extends AbstractJpaRepository<Task> implements TaskRepository {
    public JpaTaskRepository(EntityManager entityManager) {
        super(entityManager, Task.class);
    }

    @Override
    public Optional<Task> findByProjectAndName(Project project, String name) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.project = :project AND t.name = :name", Task.class)
                .setParameter("project", project)
                .setParameter("name", name)
                .getResultStream().findFirst();
    }

    @Override
    public boolean existsByProjectAndName(Project project, String name) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.project = :project AND t.name = :name", Task.class)
                .setParameter("project", project)
                .setParameter("name", name)
                .getResultStream().findFirst().isPresent();
    }

    @Override
    public boolean existsByProjectAndNameAndIdNot(Project project, String name, int id) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.project = :project AND t.name = :name AND t.id <> :id", Task.class)
                .setParameter("id", id)
                .setParameter("project", project)
                .setParameter("name", name)
                .getResultStream().findFirst().isPresent();
    }
}
