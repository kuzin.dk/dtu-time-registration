package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.repository.jpa.AbstractJpaRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class JpaUserRepository extends AbstractJpaRepository<User> implements UserRepository {
    public JpaUserRepository(EntityManager entityManager) {
        super(entityManager, User.class);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.email=:email", User.class)
                .setParameter("email", email)
                .getResultStream().findFirst();
    }

    @Override
    public boolean existsByEmail(String email) {
        return findByEmail(email).isPresent();
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    public long countByGlobalRole(GlobalRole globalRole) {
        return entityManager.createQuery("SELECT count(u) FROM User u WHERE u.globalRole = :globalRole", Long.class)
                .setParameter("globalRole", globalRole)
                .getSingleResult();
    }
}
