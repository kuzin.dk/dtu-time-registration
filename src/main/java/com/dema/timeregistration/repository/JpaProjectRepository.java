package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.repository.jpa.AbstractJpaRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

public class JpaProjectRepository extends AbstractJpaRepository<Project> implements ProjectRepository {
    public JpaProjectRepository(EntityManager entityManager) {
        super(entityManager, Project.class);
    }

    @Override
    public Optional<Project> findByCompanyAndName(Company company, String name) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.name=:name AND p.company=:company", Project.class)
                .setParameter("name", name)
                .setParameter("company", company)
                .getResultStream().findFirst();
    }

    @Override
    public boolean existsByCompanyAndName(Company company, String name) {
        return findByCompanyAndName(company, name).isPresent();
    }

    @Override
    public boolean existsByCompanyAndNameAndIdNot(Company company, String name, int id) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.name=:name AND p.company=:company AND p.id <> :id", Project.class)
                .setParameter("name", name)
                .setParameter("company", company)
                .setParameter("id", id)
                .getResultStream().findFirst().isPresent();
    }
}
