package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.Project;

import java.util.Optional;

public interface ProjectRepository extends CrudRepository<Project> {
    Optional<Project> findByCompanyAndName(Company company, String name);

    boolean existsByCompanyAndName(Company company, String name);

    boolean existsByCompanyAndNameAndIdNot(Company company, String name, int id);
}
