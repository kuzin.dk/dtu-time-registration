package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.repository.jpa.AbstractJpaRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class JpaCompanyRepository extends AbstractJpaRepository<Company> implements CompanyRepository {
    public JpaCompanyRepository(EntityManager entityManager) {
        super(entityManager, Company.class);
    }

    @Override
    public Optional<Company> findByName(String name) {
        return entityManager
                .createQuery("SELECT c FROM Company c WHERE c.name=:name", Company.class)
                .setParameter("name", name)
                .getResultStream().findFirst();
    }

    @Override
    public boolean existsByName(String name) {
        return findByName(name).isPresent();
    }

    @Override
    public boolean existsByNameAndIdNot(String name, int id) {
        return entityManager
                .createQuery("SELECT c FROM Company c WHERE c.name = :name AND c.id <> :id", Company.class)
                .setParameter("name", name)
                .setParameter("id", id)
                .getResultStream().findFirst().isPresent();
    }

    @Override
    public List<Company> findAll() {
        return entityManager.createQuery("SELECT c FROM Company c", Company.class).getResultList();
    }
}
