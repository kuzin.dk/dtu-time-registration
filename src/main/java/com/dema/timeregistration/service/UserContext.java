package com.dema.timeregistration.service;

import com.dema.timeregistration.entity.User;

import java.time.Instant;

public class UserContext {
    private Instant loginDate;
    private User user;

    public UserContext(Instant loginDate, User user) {
        this.loginDate = loginDate;
        this.user = user;
    }

    public Instant getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Instant loginDate) {
        this.loginDate = loginDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
