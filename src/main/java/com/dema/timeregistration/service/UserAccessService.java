package com.dema.timeregistration.service;

import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.entity.ProjectRole;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.exception.AccessDeniedException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;

import java.util.Optional;

import java.util.Set;

public class UserAccessService {
    private final UserRepository userRepository;

    public UserAccessService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isUserGlobalAdmin(int userId) throws EntityNotFoundException {
        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::userNotFound);
        return user.getGlobalRole().equals(GlobalRole.GLOBAL_ADMINISTRATOR);
    }

    public void throwIfUserIsNotAdmin(UserContext userContext) throws EntityNotFoundException, AccessDeniedException {
        if (!isUserGlobalAdmin(userContext.getUser().getId())) {
            throw AccessDeniedException.requiresAdministratorRole();
        }
    }

    public void throwIfUserIsNotProjectLeaderOrAdmin(Project project, UserContext userContext) throws AccessDeniedException {
        Optional<ProjectRole> projectRoleOptional = userContext.getUser().getProjectRoles()
                .stream()
                .filter(pr -> pr.getProject().equals(project) && pr.getRole().equals(Role.PROJECT_LEADER))
                .findFirst();

        if (!projectRoleOptional.isPresent() && !userContext.getUser().getGlobalRole().equals(GlobalRole.GLOBAL_ADMINISTRATOR)) {
            throw AccessDeniedException.projectLeaderOrAdministratorRole();
        }
    }

    public void throwIfUserDoesNotHaveProjectRole(Project project, UserContext userContext) throws AccessDeniedException {
        Optional<ProjectRole> projectRoleOptional = userContext.getUser().getProjectRoles()
                .stream()
                .filter(pr -> pr.getProject().equals(project))
                .findFirst();

        if (!projectRoleOptional.isPresent() && !userContext.getUser().getGlobalRole().equals(GlobalRole.GLOBAL_ADMINISTRATOR)) {
            throw AccessDeniedException.projectRoleOrAdministratorRole();
        }
    }
}
