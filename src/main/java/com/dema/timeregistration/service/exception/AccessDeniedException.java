package com.dema.timeregistration.service.exception;

import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.Role;

public class AccessDeniedException extends Exception {
    public AccessDeniedException(String message) {
        super(message);
    }

    public static AccessDeniedException requiresAdministratorRole() {
        return new AccessDeniedException("The specified action requires the role: " + GlobalRole.GLOBAL_ADMINISTRATOR.getName());
    }

    public static AccessDeniedException projectLeaderOrAdministratorRole() {
        return new AccessDeniedException("The specified action requires the role: " + Role.PROJECT_LEADER.getName() + ", or the role: " + GlobalRole.GLOBAL_ADMINISTRATOR);
    }

    public static AccessDeniedException projectRoleOrAdministratorRole() {
        return new AccessDeniedException("The specified action a role in the project, or the role: " + GlobalRole.GLOBAL_ADMINISTRATOR);
    }
}
