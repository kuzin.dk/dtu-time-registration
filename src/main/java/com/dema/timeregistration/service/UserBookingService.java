package com.dema.timeregistration.service;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.exception.AccessDeniedException;
import com.dema.timeregistration.service.exception.EntityAlreadyExistsException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;
import com.dema.timeregistration.service.models.BookUserRequest;
import com.dema.timeregistration.service.models.RegisterUserAbsenceRequest;

import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class UserBookingService {
    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;
    private final UserAccessService userAccessService;

    public UserBookingService(UserRepository userRepository, ProjectRepository projectRepository, UserAccessService userAccessService) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.userAccessService = userAccessService;
    }

    public String listUserBookings(String fromTime, String toTime, int userId) throws EntityNotFoundException, ParseException {
        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::userNotFound);

        List<UserBooking> userBookings = new ArrayList<>();
        userBookings.addAll(user.getUserProjectBookings());
        userBookings.addAll(user.getUserAbsences());

        Instant from = Utils.getDateFormat().parse(fromTime).toInstant();
        Instant to = Utils.getDateFormat().parse(toTime).toInstant();

        userBookings = userBookings.stream().filter(ub -> Utils.doTimeIntervalsOverlap(ub.getStartTime(), ub.getEndTime(), from, to)).collect(Collectors.toList());

        return userBookings.stream().map(ub -> {
            String start = Utils.getDateFormat().format(Date.from(ub.getStartTime()));
            String end = Utils.getDateFormat().format(Date.from(ub.getEndTime()));

            if (ub instanceof UserProjectBooking) {
                return "Project Booking: " + ((UserProjectBooking) ub).getProject().getName() + " - " + start + " to " + end;
            } else {
                return "Absence: " + start + " to " + end;
            }
        }).collect(Collectors.joining("\n"));
    }


    @SuppressWarnings("Duplicates")
    public void registerUserAbsence(RegisterUserAbsenceRequest request, UserContext userContext) throws ParseException, EntityAlreadyExistsException {
        Instant startTime = Utils.getDateFormat().parse(request.getStartTime()).toInstant();
        Instant endTime = Utils.getDateFormat().parse(request.getEndTime()).toInstant();

        List<UserBooking> userBookings = getAllUserBookings(userContext.getUser());

        for (UserBooking userBooking : userBookings) {
            if (Utils.doTimeIntervalsOverlap(userBooking.getStartTime(), userBooking.getEndTime(), startTime, endTime)) {
                throw new EntityAlreadyExistsException("The specified period is already booked");
            }
        }

        UserAbsence userAbsence = UserAbsence.create(userContext.getUser(), startTime, endTime, request.getReason());
        userContext.getUser().getUserAbsences().add(userAbsence);
        userRepository.save(userContext.getUser());
    }

    @SuppressWarnings("Duplicates")
    public void bookUser(BookUserRequest request, UserContext userContext) throws EntityNotFoundException, AccessDeniedException, ParseException, EntityAlreadyExistsException {
        Project project = projectRepository.findById(request.getProjectId()).orElseThrow(EntityNotFoundException::projectNotFound);
        userAccessService.throwIfUserDoesNotHaveProjectRole(project, userContext);

        User user = userRepository.findById(request.getUserId()).orElseThrow(EntityNotFoundException::userNotFound);

        Instant startTime = Utils.getDateFormat().parse(request.getStartTime()).toInstant();
        Instant endTime = Utils.getDateFormat().parse(request.getEndTime()).toInstant();

        List<UserBooking> userBookings = getAllUserBookings(user);

        for (UserBooking userBooking : userBookings) {
            if (Utils.doTimeIntervalsOverlap(userBooking.getStartTime(), userBooking.getEndTime(), startTime, endTime)) {
                throw new EntityAlreadyExistsException("The specified period is already booked");
            }
        }

        user.getUserProjectBookings().add(UserProjectBooking.create(user, project, startTime, endTime));
        userRepository.save(user);
    }

    private List<UserBooking> getAllUserBookings(User user) {
        List<UserBooking> userBookings = new ArrayList<>();
        userBookings.addAll(user.getUserAbsences());
        userBookings.addAll(user.getUserProjectBookings());
        return userBookings;
    }
}

