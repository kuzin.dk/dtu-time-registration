package com.dema.timeregistration.service;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.exception.AccessDeniedException;
import com.dema.timeregistration.service.exception.EntityAlreadyExistsException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;
import com.dema.timeregistration.service.models.CreateProjectRequest;
import com.dema.timeregistration.service.models.UpdateProjectRequest;

import java.text.ParseException;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ProjectService {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final ProjectRepository projectRepository;
    private final UserAccessService userAccessService;

    public ProjectService(UserRepository userRepository,
                          CompanyRepository companyRepository,
                          ProjectRepository projectRepository,
                          UserAccessService userAccessService) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.projectRepository = projectRepository;
        this.userAccessService = userAccessService;
    }

    public void createProject(CreateProjectRequest request, UserContext userContext) throws AccessDeniedException, EntityNotFoundException, EntityAlreadyExistsException, ParseException {
        userAccessService.throwIfUserIsNotAdmin(userContext);

        Company company = companyRepository.findById(request.getCompanyId()).orElseThrow(EntityNotFoundException::companyNotFound);

        if (projectRepository.existsByCompanyAndName(company, request.getName())) {
            throw new EntityAlreadyExistsException("A project with the specified name already exists");
        }

        User projectLeader = userRepository.findById(request.getProjectLeaderId()).orElseThrow(EntityNotFoundException::userNotFound);

        Instant dueDate = Utils.getDateFormat().parse(request.getDueDate()).toInstant();

        Project project = Project.create(request.getName(), company, dueDate, projectLeader);
        projectRepository.save(project);
        userRepository.save(projectLeader);
    }

    public void updateProject(UpdateProjectRequest request, UserContext userContext) throws AccessDeniedException, EntityNotFoundException, EntityAlreadyExistsException, ParseException {
        userAccessService.throwIfUserIsNotAdmin(userContext);


        Project project = projectRepository.findById(request.getId()).orElseThrow(EntityNotFoundException::projectNotFound);
        Company company = project.getCompany();

        if (projectRepository.existsByCompanyAndNameAndIdNot(company, request.getName(), request.getId())) {
            throw new EntityAlreadyExistsException("A project with the specified name already exists");
        }

        if (request.getName() != null) {
            project.setName(request.getName());
        }

        if (request.getDueDate() != null) {
            Instant dueDate = Utils.getDateFormat().parse(request.getDueDate()).toInstant();
            project.setDueDate(dueDate);
        }

        projectRepository.save(project);
    }

    @SuppressWarnings("Duplicates")
    public void assignProjectRole(int userId, int projectId, Role role, UserContext userContext) throws AccessDeniedException, EntityNotFoundException {
        Project project = projectRepository.findById(projectId).orElseThrow(EntityNotFoundException::projectNotFound);
        userAccessService.throwIfUserIsNotProjectLeaderOrAdmin(project, userContext);

        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::userNotFound);

        Optional<ProjectRole> projectRoleOptional = user.getProjectRoles().stream().filter(pr -> pr.getProject().equals(project)).findFirst();

        Set<ProjectRole> projectLeaders = project.getProjectRoles().stream().filter(pr -> pr.getRole().equals(Role.PROJECT_LEADER)).collect(Collectors.toSet());

        if (projectRoleOptional.isPresent()) {
            // If the user is the only project leader in the project, and we're trying to downgrade him
            if (projectRoleOptional.get().getRole().equals(Role.PROJECT_LEADER) &&
                    role != Role.PROJECT_LEADER &&
                    projectLeaders.size() == 1) {
                throw new AccessDeniedException("There must be at least one project leader for a project");
            }

            projectRoleOptional.get().setRole(role);
        } else {
            user.getProjectRoles().add(ProjectRole.create(role, project, user));
        }

        userRepository.save(user);
    }

    @SuppressWarnings("Duplicates")
    public void removeProjectRole(int userId, int projectId, UserContext userContext) throws EntityNotFoundException, AccessDeniedException {
        Project project = projectRepository.findById(projectId).orElseThrow(EntityNotFoundException::projectNotFound);
        userAccessService.throwIfUserIsNotProjectLeaderOrAdmin(project, userContext);

        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::userNotFound);
        Optional<ProjectRole> projectRoleOptional = user.getProjectRoles().stream().filter(pr -> pr.getProject().equals(project)).findFirst();

        if (!projectRoleOptional.isPresent()) {
            return;
        }

        Set<ProjectRole> projectLeaders = project.getProjectRoles().stream().filter(pr -> pr.getRole().equals(Role.PROJECT_LEADER)).collect(Collectors.toSet());

        if (projectRoleOptional.get().getRole().equals(Role.PROJECT_LEADER) && projectLeaders.size() == 1) {
            throw new AccessDeniedException("There must be at least one project leader for a project");
        }

        user.getProjectRoles().remove(projectRoleOptional.get());
        userRepository.save(user);
    }

    public int getProjectId(String companyName, String projectName) throws EntityNotFoundException {
        Company company = companyRepository.findByName(companyName).orElseThrow(EntityNotFoundException::companyNotFound);
        Project project = projectRepository.findByCompanyAndName(company, projectName).orElseThrow(EntityNotFoundException::projectNotFound);
        return project.getId();
    }

    public String listProjects(int companyId) throws EntityNotFoundException {
        Company company = companyRepository.findById(companyId).orElseThrow(EntityNotFoundException::companyNotFound);
        return company.getProjects()
                .stream()
                .map(Project::getName)
                .collect(Collectors.joining("\n"));
    }
}
