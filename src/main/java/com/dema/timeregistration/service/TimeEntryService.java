package com.dema.timeregistration.service;


import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.Task;
import com.dema.timeregistration.entity.TimeEntry;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.entity.UserProjectBooking;
import com.dema.timeregistration.repository.TaskRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.exception.AccessDeniedException;
import com.dema.timeregistration.service.exception.EntityAlreadyExistsException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;
import com.dema.timeregistration.service.models.RegisterTimeEntryRequest;
import com.dema.timeregistration.service.models.UpdateTimeEntryRequest;

import java.sql.Date;
import java.text.ParseException;
import java.time.Instant;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class TimeEntryService {
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    public TimeEntryService(UserRepository userRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    public void registerTimeEntry(RegisterTimeEntryRequest request, UserContext userContext) throws EntityNotFoundException, ParseException, AccessDeniedException, EntityAlreadyExistsException {
        User user = userRepository.findById(userContext.getUser().getId()).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findById(request.getTaskId()).orElseThrow(EntityNotFoundException::taskNotFound);

        Instant startTime = Utils.getDateFormat().parse(request.getStartTime()).toInstant();
        Instant endTime = Utils.getDateFormat().parse(request.getEndTime()).toInstant();

        if (endTime.isBefore(startTime)) {
            throw new IllegalArgumentException("The specified end time is before the start time");
        }

        Optional<UserProjectBooking> projectBookingOptional = user.getUserProjectBookings().stream()
                .filter(pb -> pb.getProject().equals(task.getProject()) && (pb.getStartTime().isBefore(startTime) || pb.getStartTime().equals(startTime)) && (pb.getEndTime().isAfter(endTime) || pb.getEndTime().equals(endTime)))
                .findFirst();

        if (!projectBookingOptional.isPresent()) {
            throw new AccessDeniedException("The time entry is not in a period where the user has been booked to the project '" + task.getProject().getName() + "'");
        }

        Optional<TimeEntry> alreadyTakenOptional = projectBookingOptional.get().getTimeEntries()
                .stream()
                .filter(te -> te.getUserProjectBooking().getUser().equals(user))
                .filter(te -> Utils.doTimeIntervalsOverlap(te.getStartTime(), te.getEndTime(), startTime, endTime))
                .findFirst();

        if (alreadyTakenOptional.isPresent()) {
            throw getOverlapsException(alreadyTakenOptional.get());
        }

        TimeEntry timeEntry = TimeEntry.create(task, projectBookingOptional.get(), startTime, endTime);
        projectBookingOptional.get().getTimeEntries().add(timeEntry);
        userRepository.save(user);
    }

    public void updateTimeEntry(UpdateTimeEntryRequest request, UserContext userContext) throws EntityNotFoundException, ParseException, EntityAlreadyExistsException, AccessDeniedException {
        User user = userRepository.findById(userContext.getUser().getId()).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findById(request.getTaskId()).orElseThrow(EntityNotFoundException::taskNotFound);

        Instant origStartTime = Utils.getDateFormat().parse(request.getOriginalStartTime()).toInstant();

        UserProjectBooking projectBooking = user.getUserProjectBookings().stream()
                .filter(pb -> pb.getProject().equals(task.getProject()) &&
                        (pb.getStartTime().isBefore(origStartTime) || pb.getStartTime().equals(origStartTime)) && (pb.getEndTime().isAfter(origStartTime)))
                .findFirst().orElseThrow(EntityNotFoundException::projectNotFound);


        TimeEntry timeEntry = projectBooking.getTimeEntries()
                .stream()
                .filter(te -> te.getStartTime().equals(origStartTime))
                .findFirst().orElseThrow(() -> new EntityNotFoundException("The specified time entry could not be found"));

        if (timeEntry.getStartTime() != null) {
            Instant startTime = Utils.getDateFormat().parse(request.getStartTime()).toInstant();
            timeEntry.setStartTime(startTime);
        }

        if (timeEntry.getEndTime() != null) {
            Instant endTime = Utils.getDateFormat().parse(request.getEndTime()).toInstant();
            timeEntry.setEndTime(endTime);
        }

        Optional<TimeEntry> alreadyTakenOptional = timeEntry.getUserProjectBooking().getTimeEntries()
                .stream()
                .filter(te -> te.getUserProjectBooking().getUser().equals(user))
                .filter(te -> Utils.doTimeIntervalsOverlap(te.getStartTime(), te.getEndTime(), timeEntry.getStartTime(), timeEntry.getEndTime()) && !te.equals(timeEntry))
                .findFirst();

        if (alreadyTakenOptional.isPresent()) {
            throw getOverlapsException(alreadyTakenOptional.get());
        }

        if (!((projectBooking.getStartTime().isBefore(timeEntry.getStartTime()) || projectBooking.getStartTime().equals(timeEntry.getStartTime())) && (projectBooking.getEndTime().isAfter(timeEntry.getEndTime()) || projectBooking.getEndTime().equals(timeEntry.getEndTime())))) {
            throw new AccessDeniedException("The time entry must be within the project booking");
        }
        userRepository.save(user);
    }

    public EntityAlreadyExistsException getOverlapsException(TimeEntry alreadyTaken) throws EntityAlreadyExistsException {
        String startTimeString = Utils.getDateFormat().format(Date.from(alreadyTaken.getStartTime()));
        String endTimeString = Utils.getDateFormat().format(Date.from(alreadyTaken.getEndTime()));

        throw new EntityAlreadyExistsException("The requested time entry overlaps with another Time Entry (" +
                startTimeString + " to " + endTimeString
                + " for task '" + alreadyTaken.getTask().getName() + "')");
    }

    public String listTimeEntries(String fromTime, String toTime, UserContext userContext) throws ParseException, EntityNotFoundException {
        Instant from = Utils.getDateFormat().parse(fromTime).toInstant();
        Instant to = Utils.getDateFormat().parse(toTime).toInstant();

        User user = userRepository.findById(userContext.getUser().getId()).orElseThrow(EntityNotFoundException::userNotFound);

        return user.getUserProjectBookings().stream()
                .flatMap(pb -> pb.getTimeEntries().stream())
                .filter(te -> te.getStartTime().isAfter(from) && te.getEndTime().isBefore(to))
                .sorted(Comparator.comparing(TimeEntry::getStartTime))
                .map(te -> {
                    String startTimeString = Utils.getDateFormat().format(Date.from(te.getStartTime()));
                    String endTimeString = Utils.getDateFormat().format(Date.from(te.getEndTime()));

                    return te.getTask().getName() + " - " + startTimeString + " to " + endTimeString;
                }).collect(Collectors.joining("\n"));
    }
}


