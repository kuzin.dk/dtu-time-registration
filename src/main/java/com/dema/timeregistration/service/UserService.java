package com.dema.timeregistration.service;

import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.exception.EntityAlreadyExistsException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;

import java.time.Instant;
import java.util.stream.Collectors;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void register(String email, String name) throws EntityAlreadyExistsException {
        if (userRepository.existsByEmail(email)) {
            throw new EntityAlreadyExistsException("The email " + email + " is already taken");
        }

        User user = User.create(email, name, GlobalRole.GLOBAL_USER);
        userRepository.save(user);
    }

    public UserContext login(String email) throws EntityNotFoundException {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("A user with the email '" + email + "' could not be found"));
        return new UserContext(Instant.now(), user);
    }

    public int getUserId(String email) throws EntityNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(EntityNotFoundException::userNotFound).getId();
    }

    public String listUsers() {
        return userRepository.findAll()
                .stream()
                .map(u -> u.getName() + " (" + u.getEmail() + ")")
                .collect(Collectors.joining("\n"));
    }
}
