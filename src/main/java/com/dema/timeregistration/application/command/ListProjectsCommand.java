package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandGroup;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;
import com.dema.timeregistration.service.AdministratorService;
import com.dema.timeregistration.service.ProjectService;

import java.util.Set;

public class ListProjectsCommand implements Command {
    private final ProjectService projectService;
    private final AdministratorService administratorService;

    public ListProjectsCommand(ProjectService projectService, AdministratorService administratorService) {
        this.projectService = projectService;
        this.administratorService = administratorService;
    }

    @Override
    public String getDocumentation() {
        return "Get a list of projects for a given company with parameters:\n-c company name\nExample of usage:\nlist-projects -c \"My Company\"";
    }

    @Override
    public String getKeyword() {
        return "list-projects";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("c")
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.GENERAL;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int companyId = administratorService.getCompanyId(parsedCommand.get("c"));
        if(projectService.listProjects(companyId).isEmpty()) {
            System.out.println("Currently no projects exists for this company");
        } else {
            System.out.println(projectService.listProjects(companyId));
        }
    }
}
