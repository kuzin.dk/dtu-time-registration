package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.ProjectService;
import com.dema.timeregistration.service.models.UpdateProjectRequest;

import java.util.Set;

public class UpdateProjectCommand implements Command {
    private final ProjectService projectService;
    private final ApplicationContext applicationContext;

    public UpdateProjectCommand(ProjectService projectService, ApplicationContext applicationContext) {
        this.projectService = projectService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Update a current project with parameters:\n-o for current project name\n-c specify which company the " +
                "project originates from\n-n optional parameter to change the name of the project\n-s optional" +
                " parameter to change the due date\nExample of usage:\nupdate-project -o \"My Project\" -c \"My Company\" -n \"My New Project Name\"";
    }

    @Override
    public String getKeyword() {
        return "update-project";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.PROJECT_ADMINISTRATION;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("o"),
                new Parameter("c"),
                new Parameter("n", false),
                new Parameter("d", false)
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        UpdateProjectRequest request = new UpdateProjectRequest();

        int projectId = projectService.getProjectId(parsedCommand.get("c"), parsedCommand.get("o"));

        request.setId(projectId);
        parsedCommand.getOptional("n").ifPresent(request::setName);
        parsedCommand.getOptional("d").ifPresent(request::setDueDate);

        projectService.updateProject(request, applicationContext.getUserContextOrThrow());
        System.out.println("Sucessfully updated project");
    }
}
