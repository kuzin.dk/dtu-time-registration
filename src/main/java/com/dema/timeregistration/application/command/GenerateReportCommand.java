package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.GenerateReportService;
import com.dema.timeregistration.service.ProjectService;
import com.dema.timeregistration.service.TaskService;
import com.dema.timeregistration.service.models.CreateTaskRequest;

import java.util.Set;

public class GenerateReportCommand implements Command {
    private final TaskService taskService;
    private final ProjectService projectService;
    private final ApplicationContext applicationContext;
    private final GenerateReportService generateReportService;

    public GenerateReportCommand(TaskService taskService, ProjectService projectService, ApplicationContext applicationContext, GenerateReportService generateReportService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.applicationContext = applicationContext;
        this.generateReportService = generateReportService;
    }

    @Override
    public String getDocumentation() {
        return "Generate a report of a given project by using the following parameters:\n-c for the company owner of" +
                " the project\n-p for the project name\nExample of usage:\ngenerate-report -c \"My Company\" -p \"My Project\"";
    }

    @Override
    public String getKeyword() {
        return "generate-report";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("p"), // project
                new Parameter("c") // company
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.PROJECT_ADMINISTRATION;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int projectId = projectService.getProjectId(parsedCommand.get("c"), parsedCommand.get("p"));
        System.out.println(generateReportService.GenerateReport(projectId));
    }
}
