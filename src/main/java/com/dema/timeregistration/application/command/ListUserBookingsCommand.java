package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandGroup;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;
import com.dema.timeregistration.service.UserBookingService;
import com.dema.timeregistration.service.UserService;

import java.util.Set;

public class ListUserBookingsCommand implements Command {
    private final UserBookingService userBookingService;
    private final UserService userService;

    public ListUserBookingsCommand(UserBookingService userBookingService, UserService userService) {
        this.userBookingService = userBookingService;
        this.userService = userService;
    }

    @Override
    public String getKeyword() {
        return "list-user-bookings";
    }

    @Override
    public String getDocumentation() {
        return "Show a list of bookings for a user using parameters:\n-e email of user\n-f from, start time of booking" +
                "\n-t -to, end time of booking\nExample of usage:\nlist-user-bookings -e \"daniel@kuzin.dk\" -f \"01/01/2019 12:00\" -t\"31/12/2019 12:00\"";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.USER_BOOKING;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("e"),
                new Parameter("f"),
                new Parameter("t")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int userId = userService.getUserId(parsedCommand.get("e"));
        if(this.userBookingService.listUserBookings(parsedCommand.get("f"), parsedCommand.get("t"), userId).isEmpty()) {
            System.out.println("No user booking could be found");
        } else {
            System.out.println(this.userBookingService.listUserBookings(parsedCommand.get("f"), parsedCommand.get("t"), userId));
        }
    }
}
