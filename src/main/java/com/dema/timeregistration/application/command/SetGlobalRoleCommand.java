package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.service.AdministratorService;
import com.dema.timeregistration.service.UserService;

import java.util.Set;

public class SetGlobalRoleCommand implements Command {
    private final AdministratorService administratorService;
    private final UserService userService;
    private final ApplicationContext applicationContext;

    public SetGlobalRoleCommand(AdministratorService administratorService, UserService userService, ApplicationContext applicationContext) {
        this.administratorService = administratorService;
        this.userService = userService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Change the global role of a user using parameters:\n-e for email\n-r for role\nThe available " +
                "global roles are:\n\"GLOBAL_ADMINISTRATOR\"\n\"GLOBAL_USER\"\nExample of usage:\n" +
                "set-global-role -e \"daniel@kuzin.dk\" -r \"GLOBAL_ADMINISTRATOR\"\nthis command is only permitted" +
                " to users with the \"GLOBAL_ADMINISTRATOR\" role";
    }

    @Override
    public String getKeyword() {
        return "set-global-role";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.ADMIN;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("e"),
                new Parameter("r")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int userId = userService.getUserId(parsedCommand.get("e"));

        GlobalRole globalRole;

        try {
            globalRole = GlobalRole.valueOf(parsedCommand.get("r"));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("The global role '" + parsedCommand.get("r") + "' does not exist");
        }

        administratorService.setGlobalRole(userId, globalRole, applicationContext.getUserContextOrThrow());

        System.out.println("User with email '" + parsedCommand.get("e") + "' has recieved role with name '" + parsedCommand.get("r") + "'");
    }
}
