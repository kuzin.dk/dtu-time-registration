package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandGroup;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;
import com.dema.timeregistration.service.AdministratorService;

import java.util.Set;

public class ListCompaniesCommand implements Command {
    private final AdministratorService administratorService;

    public ListCompaniesCommand(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    @Override
    public String getDocumentation() {
        return "Get an overview of current existing companies";
    }

    @Override
    public String getKeyword() {
        return "list-companies";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet();
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.GENERAL;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) {
        if(administratorService.listCompanies().isEmpty()) {
            System.out.println("Currently no company exists");
        } else {
            System.out.println(administratorService.listCompanies());
        }

    }
}
