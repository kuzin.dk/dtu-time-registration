package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandGroup;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;
import com.dema.timeregistration.service.UserService;

import java.util.Set;

public class RegisterCommand implements Command {
    private final UserService userService;

    public RegisterCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String getDocumentation() {
        return "Create a new user using parameters:\n-n for name\n-e for email\n" +
                "Example of usage:\nregister -n \"Daniel Kuzin\" -e \"daniel@kuzin.dk\"";
    }

    @Override
    public String getKeyword() {
        return "register";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.ACCOUNT;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("n"),
                new Parameter("e")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        userService.register(parsedCommand.get("e"), parsedCommand.get("n"));
        System.out.println("The user has been registered successfully");
    }
}
