package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.ProjectService;
import com.dema.timeregistration.service.TaskService;
import com.dema.timeregistration.service.models.CreateTaskRequest;

import java.util.Set;

public class CreateTaskCommand implements Command {
    private final TaskService taskService;
    private final ProjectService projectService;
    private final ApplicationContext applicationContext;

    public CreateTaskCommand(TaskService taskService, ProjectService projectService, ApplicationContext applicationContext) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Create a task for a project using parameters\n-n for name\n-p for project\n-c for company\n-d for" +
                " description\n-s for status\n-e for estimate\nExample of usage:\ncreate-task -n \"My Task\"" +
                " -p \"My Project\" -c \"My Company\" -d \"A description\" -s \"In Progress\" -e \"PT10H\"";
    }

    @Override
    public String getKeyword() {
        return "create-task";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("n"), // name
                new Parameter("p"), // project
                new Parameter("c"), // company
                new Parameter("d"), // description
                new Parameter("s"), // status
                new Parameter("e") // estimate
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.PROJECT_ADMINISTRATION;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int projectId = projectService.getProjectId(parsedCommand.get("c"), parsedCommand.get("p"));

        CreateTaskRequest request = new CreateTaskRequest();

        request.setName(parsedCommand.get("n"));
        request.setProjectId(projectId);
        request.setDescription(parsedCommand.get("d"));
        request.setStatus(parsedCommand.get("s"));
        request.setEstimate(parsedCommand.get("e"));

        taskService.createTask(request, applicationContext.getUserContextOrThrow());
        System.out.println("The task was created succesfully");
    }
}
