package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandGroup;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;
import com.dema.timeregistration.service.ProjectService;
import com.dema.timeregistration.service.TaskService;

import java.util.Set;

public class ListTasksCommand implements Command {
    private final TaskService taskService;
    private final ProjectService projectService;

    public ListTasksCommand(TaskService taskService, ProjectService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public String getDocumentation() {
        return "Get a list of current tasks for a given project with parameters:\n-c for the company that owns the" +
                " project\n-p name of the project\nExample of usage:\nlist-tasks -c \"My Company\" -p \"My Project\"";
    }

    @Override
    public String getKeyword() {
        return "list-tasks";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("c"),
                new Parameter("p")
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.GENERAL;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int projectId = projectService.getProjectId(parsedCommand.get("c"), parsedCommand.get("p"));
        if(taskService.listTasks(projectId).isEmpty()) {
            System.out.println("Currently no tasks exists for this project");
        } else {
            System.out.println(taskService.listTasks(projectId));
        }

    }
}
