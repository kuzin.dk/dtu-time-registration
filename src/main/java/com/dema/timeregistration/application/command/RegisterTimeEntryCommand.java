package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.TaskService;
import com.dema.timeregistration.service.TimeEntryService;
import com.dema.timeregistration.service.models.RegisterTimeEntryRequest;

import java.util.Set;

public class RegisterTimeEntryCommand implements Command {
    private final TimeEntryService timeEntryService;
    private final TaskService taskService;
    private final ApplicationContext applicationContext;

    public RegisterTimeEntryCommand(TimeEntryService timeEntryService, TaskService taskService, ApplicationContext applicationContext) {
        this.timeEntryService = timeEntryService;
        this.taskService = taskService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getKeyword() {
        return "register-time";
    }

    @Override
    public String getDocumentation() {
        return "Register a time entry for a given task with following parameters:\n-c name of company\n-p name of project" +
                "\n-t name of task\n-s start time of the time entry\n-e end time of the time entry\nExample of usage:\n" +
                "register-time -c \"My Company\" -p \"My Project\" -t \"My Task\" -s \"01/01/2019 12:00\" -e \"31/12/2019 12:00\"";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("c"),
                new Parameter("p"),
                new Parameter("t"),
                new Parameter("s"),
                new Parameter("e")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int projectId = taskService.getTaskId(parsedCommand.get("c"), parsedCommand.get("p"), parsedCommand.get("t"));

        RegisterTimeEntryRequest request = new RegisterTimeEntryRequest();
        request.setTaskId(projectId);
        request.setStartTime(parsedCommand.get("s"));
        request.setEndTime(parsedCommand.get("e"));

        timeEntryService.registerTimeEntry(request, applicationContext.getUserContextOrThrow());

        System.out.println("Success");
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.TIME_ENTRY;
    }
}
