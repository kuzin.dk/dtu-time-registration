package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.TaskService;
import com.dema.timeregistration.service.models.UpdateTaskRequest;

import java.util.Set;

public class UpdateTaskCommand implements Command {
    private final TaskService taskService;
    private final ApplicationContext applicationContext;

    public UpdateTaskCommand(TaskService taskService, ApplicationContext applicationContext) {
        this.taskService = taskService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Update a current task by naming the task name and using one of the following parameters, that you wish to update:\n" +
                "-o the task you wish to update\n-n update task name\n-p specify which project the task originates from\n" +
                "-c specify which company the project originates from\n-d optional parameter to update the description\n" +
                "-s optional parameter to update the status\n-e optional parameter to update the estimated duration of the task\n" +
                "Example of usage:\nupdate-task -o \"My Task\" -p \"My Project\" -c \"My Company\" -s \"Completed\"";
    }

    @Override
    public String getKeyword() {
        return "update-task";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.PROJECT_ADMINISTRATION;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("o"), // original name
                new Parameter("n", false), // name
                new Parameter("p", false), // project
                new Parameter("c", false), // company
                new Parameter("d", false), // description
                new Parameter("s", false), // status
                new Parameter("e", false) // estimate
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int taskId = taskService.getTaskId(parsedCommand.get("c"), parsedCommand.get("p"), parsedCommand.get("o"));

        UpdateTaskRequest request = new UpdateTaskRequest();

        request.setId(taskId);

        parsedCommand.getOptional("n").ifPresent(request::setName);
        parsedCommand.getOptional("d").ifPresent(request::setDescription);
        parsedCommand.getOptional("s").ifPresent(request::setStatus);
        parsedCommand.getOptional("e").ifPresent(request::setEstimate);

        taskService.updateTask(request, applicationContext.getUserContextOrThrow());

        System.out.println("The task was updated successfully");
    }
}
