package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.AdministratorService;

import java.util.Set;

public class CreateCompanyCommand implements Command {
    private final AdministratorService administratorService;
    private final ApplicationContext applicationContext;

    public CreateCompanyCommand(AdministratorService administratorService, ApplicationContext applicationContext) {
        this.administratorService = administratorService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Create a company using parameters:\n-n for name\nExample for usage:\ncreate-company -n \"Softwarehuset\"";
    }

    @Override
    public String getKeyword() {
        return "create-company";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.ADMIN;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("n")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        administratorService.createCompany(parsedCommand.get("n"), applicationContext.getUserContextOrThrow());
        System.out.println("The company was created successfully");
    }
}
