package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.UserService;
import com.dema.timeregistration.service.exception.EntityNotFoundException;

import java.util.Set;

public class LoginCommand implements Command {
    private final UserService userService;
    private final ApplicationContext applicationContext;

    public LoginCommand(UserService userService, ApplicationContext applicationContext) {
        this.userService = userService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Login with an existing user using parameter\n-e for email\nExample of usage:\nlogin -e \"user@test.com\"";
    }

    @Override
    public String getKeyword() {
        return "login";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.ACCOUNT;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("e")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws EntityNotFoundException {
        applicationContext.setUserContext(userService.login(parsedCommand.get("e")));
        System.out.println("Login succeeded");
    }
}
