package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.AdministratorService;
import com.dema.timeregistration.service.ProjectService;
import com.dema.timeregistration.service.UserService;
import com.dema.timeregistration.service.models.CreateProjectRequest;

import java.util.Set;

public class CreateProjectCommand implements Command {
    private final AdministratorService administratorService;
    private final UserService userService;
    private final ProjectService projectService;
    private final ApplicationContext applicationContext;

    public CreateProjectCommand(AdministratorService administratorService, UserService userService, ProjectService projectService, ApplicationContext applicationContext) {
        this.administratorService = administratorService;
        this.userService = userService;
        this.projectService = projectService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Create a new project for a company with parameters:\n-c name of the company that owns the project\n-n " +
                "name of the new project\n-l email of the project leader\n-d due date for the project\nExample of usage:\ncreate-project -c " +
                "\"My Company\" -n \"My Project\" -l \"admin@app\" -d \"30/12/2019 12:00\"";
    }

    @Override
    public String getKeyword() {
        return "create-project";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("c"),
                new Parameter("n"),
                new Parameter("l"),
                new Parameter("d")
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.PROJECT_ADMINISTRATION;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        CreateProjectRequest request = new CreateProjectRequest();

        int companyId = administratorService.getCompanyId(parsedCommand.get("c"));
        int projectLeaderId = userService.getUserId(parsedCommand.get("l"));

        request.setName(parsedCommand.get("n"));
        request.setCompanyId(companyId);
        request.setProjectLeaderId(projectLeaderId);
        request.setDueDate(parsedCommand.get("d"));

        projectService.createProject(request, applicationContext.getUserContextOrThrow());
        System.out.println("The project was created succesfully");
    }
}
