package com.dema.timeregistration.application.command;

import com.dema.timeregistration.application.ApplicationContext;
import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;

import java.util.HashSet;
import java.util.Set;

public class ExitCommand implements Command {
    private final ApplicationContext applicationContext;

    public ExitCommand(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Quit the current session";
    }

    @Override
    public String getKeyword() {
        return "exit";
    }

    @Override
    public Set<Parameter> getParameters() {
        return new HashSet<>();
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) {
        applicationContext.setShutDown(true);
    }
}
