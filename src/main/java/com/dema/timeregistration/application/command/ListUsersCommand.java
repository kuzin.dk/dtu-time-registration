package com.dema.timeregistration.application.command;

import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandGroup;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;
import com.dema.timeregistration.service.UserService;

import java.util.HashSet;
import java.util.Set;

public class ListUsersCommand implements Command {
    private final UserService userService;

    public ListUsersCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String getDocumentation() {
        return "Get an overview of current existing users";
    }

    @Override
    public String getKeyword() {
        return "list-users";
    }

    @Override
    public Set<Parameter> getParameters() {
        return new HashSet<>();
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) {
        if(userService.listUsers().isEmpty()) {
            System.out.println("No users to be found");
        } else {
            System.out.println(userService.listUsers());
        }
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.GENERAL;
    }
}
