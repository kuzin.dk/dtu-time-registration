package com.dema.timeregistration.application;

import java.util.Objects;

public class Parameter {
    private final String name;
    private final boolean required;

    public Parameter(String name, boolean required) {
        this.name = name;
        this.required = required;
    }

    public Parameter(String name) {
        this.name = name;
        this.required = true;
    }

    public String getName() {
        return name;
    }

    public boolean isRequired() {
        return required;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Parameter)) return false;

        Parameter parameter = (Parameter) o;

        if (required != parameter.required) return false;
        return Objects.equals(name, parameter.name);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (required ? 1 : 0);
        return result;
    }
}
