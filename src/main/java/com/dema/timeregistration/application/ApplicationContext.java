package com.dema.timeregistration.application;

import com.dema.timeregistration.service.UserContext;
import com.dema.timeregistration.service.exception.AccessDeniedException;

public class ApplicationContext {
    private UserContext userContext;
    private boolean isShutDown = false;

    public UserContext getUserContextOrThrow() throws AccessDeniedException {
        if(userContext == null) {
            throw new AccessDeniedException("Please log in to continue");
        }

        return userContext;
    }

    public void setUserContext(UserContext userContext) {
        this.userContext = userContext;
    }

    public boolean isShutDown() {
        return isShutDown;
    }

    public void setShutDown(boolean shutDown) {
        isShutDown = shutDown;
    }
}
