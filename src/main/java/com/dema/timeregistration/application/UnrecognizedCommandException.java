package com.dema.timeregistration.application;

public class UnrecognizedCommandException extends Exception {
    public UnrecognizedCommandException(String message) {
        super(message);
    }
}
