package com.dema.timeregistration.application;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.command.*;
import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.repository.*;
import com.dema.timeregistration.repository.jpa.JpaRepositoryConfiguration;
import com.dema.timeregistration.service.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        Path dbFolder = Paths.get("./db");

        if (!Files.exists(dbFolder)) {
            Files.createDirectory(dbFolder);
            System.out.println("Created Directory " + dbFolder.toAbsolutePath().toString());
        }

        ApplicationContext applicationContext = new ApplicationContext();

        // Instantiate Dependencies
        EntityManager entityManager = Persistence.createEntityManagerFactory("timeregistration", JpaRepositoryConfiguration.properties).createEntityManager();

        UserRepository userRepository = new JpaUserRepository(entityManager);
        CompanyRepository companyRepository = new JpaCompanyRepository(entityManager);
        TaskRepository taskRepository = new JpaTaskRepository(entityManager);
        ProjectRepository projectRepository = new JpaProjectRepository(entityManager);

        UserAccessService userAccessService = new UserAccessService(userRepository);
        AdministratorService administratorService = new AdministratorService(userRepository, companyRepository, userAccessService);
        UserService userService = new UserService(userRepository);
        ProjectService projectService = new ProjectService(userRepository, companyRepository, projectRepository, userAccessService);
        TaskService taskService = new TaskService(taskRepository, projectRepository, companyRepository, userAccessService);
        TimeEntryService timeEntryService = new TimeEntryService(userRepository, taskRepository);
        GenerateReportService generateReportService = new GenerateReportService(projectRepository);
        UserBookingService userBookingService = new UserBookingService(userRepository, projectRepository, userAccessService);

        // Initial Data
        if (!userRepository.findByEmail("admin@app").isPresent()) {
            User admin = User.create("admin@app", "Administrator", GlobalRole.GLOBAL_ADMINISTRATOR);
            userRepository.save(admin);
        }

        // The DocumentationCommand needs a reference to the CommandInterpreter and is therefore populated with it
        // after instantiation
        DocumentationCommand documentationCommand = new DocumentationCommand();

        // Register commands that will be interpreted by the CommandInterpreter
        Set<Command> commands = Utils.newHashSet(
                new LoginCommand(userService, applicationContext),
                new RegisterCommand(userService),

                new ListUsersCommand(userService),
                new ListCompaniesCommand(administratorService),
                new ListProjectsCommand(projectService, administratorService),
                new ListTasksCommand(taskService, projectService),
                new ListTimeEntriesCommand(timeEntryService, applicationContext),
                new ListUserBookingsCommand(userBookingService, userService),

                new CreateProjectCommand(administratorService, userService, projectService, applicationContext),
                new UpdateProjectCommand(projectService, applicationContext),
                new GenerateReportCommand(taskService, projectService, applicationContext, generateReportService),

                new CreateTaskCommand(taskService, projectService, applicationContext),
                new UpdateTaskCommand(taskService, applicationContext),

                new RegisterTimeEntryCommand(timeEntryService, taskService, applicationContext),
                new UpdateTimeEntryCommand(timeEntryService, taskService, applicationContext),

                new RegisterUserAbcenseCommand(userBookingService, applicationContext),
                new BookUserCommand(userBookingService, userService, projectService, applicationContext),

                new CreateCompanyCommand(administratorService, applicationContext),
                new UpdateCompanyCommand(administratorService, applicationContext),
                new SetGlobalRoleCommand(administratorService, userService, applicationContext),

                documentationCommand,
                new ExitCommand(applicationContext)
        );

        CommandInterpreter commandInterpreter = new CommandInterpreter(commands);

        documentationCommand.setCommandInterpreter(commandInterpreter);

        try (Scanner s = new Scanner(System.in, "UTF-8")) {
            System.out.println("Ready for commands (use the 'help' command to view a list of available commands): ");

            while (!applicationContext.isShutDown()) {
                String line = s.nextLine();

                try {
                    ParsedCommand parsedCommand = commandInterpreter.parseCommand(line);
                    parsedCommand.getCommand().doCommand(parsedCommand);
                } catch (Exception e) {
                    System.out.println("An error occurred (" + e.getClass().getSimpleName() + "): " + e.getMessage());
                }
            }
        }
    }
}
