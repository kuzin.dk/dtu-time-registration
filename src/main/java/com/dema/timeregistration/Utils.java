package com.dema.timeregistration;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

public class Utils {
    private Utils() {
    }

    public static SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.forLanguageTag("DA"));
    }

    public static boolean doTimeIntervalsOverlap(Instant interval1Start, Instant interval1End, Instant interval2Start, Instant interval2End) {
        return (interval2Start.isAfter(interval1Start) && interval2Start.isBefore(interval1End)) ||
                interval1Start.equals(interval2Start) ||
                (interval2End.isAfter(interval1Start) && interval2End.isBefore(interval1End)) ||
                interval1End.equals(interval2End) ||
                (interval2Start.isBefore(interval1Start) && interval2End.isAfter(interval1End));
    }

    public static <T> HashSet<T> newHashSet(T... values) {
        return new HashSet<>(Arrays.asList(values));
    }
}
