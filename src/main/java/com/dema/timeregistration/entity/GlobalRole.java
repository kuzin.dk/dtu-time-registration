package com.dema.timeregistration.entity;

public enum GlobalRole {
    GLOBAL_ADMINISTRATOR("Global Administrator"),
    GLOBAL_USER("User");

    private final String name;

    GlobalRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
