package com.dema.timeregistration.entity;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "UserProjectBooking")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class UserProjectBooking extends UserBooking {
    @ManyToOne
    private Project project;

    @OneToMany(mappedBy = "userProjectBooking", cascade = CascadeType.ALL)
    private Set<TimeEntry> timeEntries = new HashSet<>();

    protected UserProjectBooking() {
    }

    public static UserProjectBooking create(User user, Project project, Instant startTime, Instant endTime) {
        UserProjectBooking booking = new UserProjectBooking();
        booking.setUser(user);
        booking.setProject(project);
        booking.setStartTime(startTime);
        booking.setEndTime(endTime);
        return booking;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Set<TimeEntry> getTimeEntries() {
        return timeEntries;
    }
}
