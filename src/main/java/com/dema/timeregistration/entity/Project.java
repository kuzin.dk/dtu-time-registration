package com.dema.timeregistration.entity;

import com.dema.timeregistration.repository.jpa.InstantAttributeConverter;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "company"})})
public class Project extends EntityWithId {
    @Column(name = "name")
    private String name;

    @JoinColumn(name = "company", nullable = false)
    @ManyToOne
    private Company company;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    private Set<Task> tasks = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    private Set<ProjectRole> projectRoles = new HashSet<>();

    @Convert(converter = InstantAttributeConverter.class)
    private Instant dueDate;

    protected Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public Set<ProjectRole> getProjectRoles() {
        return projectRoles;
    }

    public void setProjectRoles(Set<ProjectRole> projectRoles) {
        this.projectRoles = projectRoles;
    }

    public static Project create(String name, Company company, Instant dueDate, User projectLeader) {
        Project project = new Project();
        project.setName(name);
        project.setCompany(company);
        project.setDueDate(dueDate);

        ProjectRole pr = ProjectRole.create(Role.PROJECT_LEADER, project, projectLeader);
        projectLeader.getProjectRoles().add(pr);
        project.getProjectRoles().add(pr);
        return project;
    }
}
