package com.dema.timeregistration.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User extends EntityWithId {
    private String email;
    private String name;

    @Enumerated(EnumType.STRING)
    private GlobalRole globalRole;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<ProjectRole> projectRoles = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<UserAbsence> userAbsences = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<UserProjectBooking> userProjectBookings = new HashSet<>();

    protected User() {
    }

    public static User create(String email, String name, GlobalRole globalRole) {
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        user.setGlobalRole(globalRole);
        return user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGlobalRole(GlobalRole globalRole) {
        this.globalRole = globalRole;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public GlobalRole getGlobalRole() {
        return globalRole;
    }

    public Set<ProjectRole> getProjectRoles() {
        return projectRoles;
    }

    public Set<UserAbsence> getUserAbsences() {
        return userAbsences;
    }

    public Set<UserProjectBooking> getUserProjectBookings() {
        return userProjectBookings;
    }
}
