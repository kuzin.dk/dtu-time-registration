package com.dema.timeregistration.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.time.Instant;

@Entity(name = "UserAbsence")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class UserAbsence extends UserBooking {
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public static UserAbsence create(User user, Instant startTime, Instant endTime, String reason) {
        UserAbsence userAbsence = new UserAbsence();
        userAbsence.setUser(user);
        userAbsence.setStartTime(startTime);
        userAbsence.setEndTime(endTime);
        userAbsence.setReason(reason);
        return userAbsence;
    }
}
