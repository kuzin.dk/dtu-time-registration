package com.dema.timeregistration.entity;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"project", "user"})})
public class ProjectRole extends EntityWithId {
    @Enumerated(EnumType.STRING)
    private Role role;

    @JoinColumn(name = "project")
    @ManyToOne
    private Project project;

    @JoinColumn(name = "user")
    @ManyToOne
    private User user;

    public static ProjectRole create(Role role, Project project, User user) {
        ProjectRole projectRole = new ProjectRole();
        projectRole.setRole(role);
        projectRole.setProject(project);
        projectRole.setUser(user);
        return projectRole;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
