package com.dema.timeregistration.entity;

import com.dema.timeregistration.repository.jpa.InstantAttributeConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class TimeEntry extends EntityWithId {
    @JoinColumn(name = "task")
    @ManyToOne
    private Task task;

    @JoinColumn(name = "user_project_booking")
    @ManyToOne
    private UserProjectBooking userProjectBooking;

    @Convert(converter = InstantAttributeConverter.class)
    private Instant startTime;

    @Convert(converter = InstantAttributeConverter.class)
    private Instant endTime;

    protected TimeEntry() {
    }

    public static TimeEntry create(Task task, UserProjectBooking userProjectBooking, Instant startTime, Instant endTime) {
        TimeEntry timeEntry = new TimeEntry();
        timeEntry.setTask(task);
        timeEntry.setUserProjectBooking(userProjectBooking);
        timeEntry.setStartTime(startTime);
        timeEntry.setEndTime(endTime);
        return timeEntry;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public UserProjectBooking getUserProjectBooking() {
        return userProjectBooking;
    }

    public void setUserProjectBooking(UserProjectBooking userProjectBooking) {
        this.userProjectBooking = userProjectBooking;
    }
}
