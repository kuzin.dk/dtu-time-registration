package com.dema.timeregistration.entity;

import com.dema.timeregistration.repository.jpa.DurationAttributeConverter;

import javax.persistence.*;
import java.time.Duration;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "project"})})
public class Task extends EntityWithId {
    @Column(name = "name")
    private String name;

    @JoinColumn(name = "project")
    @ManyToOne
    private Project project;

    private String description;
    private String status;

    @Convert(converter = DurationAttributeConverter.class)
    private Duration estimate;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "task")
    private Set<TimeEntry> timeEntries;


    public static Task create(String name, Project project, String description, String status, Duration estimate) {
        Task task = new Task();
        task.setName(name);
        task.setProject(project);
        task.setDescription(description);
        task.setStatus(status);
        task.setEstimate(estimate);
        return task;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Duration getEstimate() {
        return estimate;
    }

    public void setEstimate(Duration estimate) {
        this.estimate = estimate;
    }

    public Set<TimeEntry> getTimeEntries() {
        return timeEntries;
    }
}
