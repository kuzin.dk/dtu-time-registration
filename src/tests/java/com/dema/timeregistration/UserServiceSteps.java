package com.dema.timeregistration;

import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.UserContext;
import com.dema.timeregistration.service.UserService;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class UserServiceSteps {
    private final UserService userService;
    private final UserRepository userRepository;
    private final TestExceptionHandler testExceptionHandler;

    private UserContext userContext;

    public UserServiceSteps(UserService userService,
                            UserRepository userRepository,
                            TestExceptionHandler testExceptionHandler) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.testExceptionHandler = testExceptionHandler;
    }

    @When("a user registers an account with the email address {string} and the name {string}")
    public void register(String email, String name) {
        try {
            this.userService.register(email, name);
        } catch (Exception e) {
            this.testExceptionHandler.setException(e);
        }
    }

    @Then("a user should exist with the email address {string} and the name {string}")
    public void userShouldExist(String email, String name) {
        User user = this.userRepository.findByEmail(email).orElseThrow(IllegalStateException::new);
        Assert.assertEquals(user.getName(), name);
    }

    @When("a user logs in with the email {string}")
    public void logIn(String email) {
        try {
            userContext = userService.login(email);
        } catch (Exception e) {
            this.testExceptionHandler.setException(e);
        }
    }

    @Then("a user context with a user with the email {string} should be received")
    public void userContextShouldBeReceived(String email) {
        Assert.assertNotNull(userContext);
        Assert.assertEquals(userContext.getUser().getEmail(), email);
    }
}
