package com.dema.timeregistration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JpaRepositoryTestsConfiguration {
    public static final Map<String, String> properties;

    static {
        Map<String, String> props = new HashMap<>();

        // Use H2 in memory database, due to better support for foreign keys (ALTER sql statements specifically)
        props.put("javax.persistence.jdbc.url", "jdbc:h2:mem:");

        properties = Collections.unmodifiableMap(props);
    }
}
