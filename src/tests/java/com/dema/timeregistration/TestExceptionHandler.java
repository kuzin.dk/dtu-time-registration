package com.dema.timeregistration;

import cucumber.api.java.en.Then;
import org.junit.Assert;

public class TestExceptionHandler {
    private Exception exception;

    public void setException(Exception exception) {
        this.exception = exception;
    }

    @Then("an exception of type {string} should be thrown")
    public void exceptionOfTypeThrown(String exceptionType) {
        if(exception == null) {
            throw new IllegalStateException("No exception was thrown");
        }

        Assert.assertEquals(exception.getMessage(), exceptionType, exception.getClass().getSimpleName());
        exception = null;
    }
}
