package com.dema.timeregistration.application;

import com.dema.timeregistration.Utils;

import java.util.Set;

public class TestCommand implements Command {
    private boolean hasExecuted = false;

    @Override
    public String getKeyword() {
        return "test";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("a", false),
                new Parameter("b"),
                new Parameter("c")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) {
        hasExecuted = true;
    }

    boolean hasExecuted() {
        return hasExecuted;
    }
}
