package com.dema.timeregistration.application;

import com.dema.timeregistration.Utils;
import org.junit.Assert;
import org.junit.Test;

public class CommandInterpreterTests {
    @Test
    public void testSuccessfulExecution() throws Exception {
        TestCommand command = new TestCommand();
        CommandInterpreter commandInterpreter = new CommandInterpreter(Utils.newHashSet(command));
        ParsedCommand parsedCommand = commandInterpreter.parseCommand("test -a test1 -b \"test2\" -c \"test 3\"");

        Assert.assertEquals(parsedCommand.get("a"), "test1");
        Assert.assertEquals(parsedCommand.get("b"), "test2");
        Assert.assertEquals(parsedCommand.get("c"), "test 3");

        parsedCommand.getCommand().doCommand(parsedCommand);

        Assert.assertTrue(command.hasExecuted());
    }

    @Test(expected = UnrecognizedCommandException.class)
    public void testMissingParameter() throws UnrecognizedCommandException {
        TestCommand command = new TestCommand();
        CommandInterpreter commandInterpreter = new CommandInterpreter(Utils.newHashSet(command));
        commandInterpreter.parseCommand("test -a test1 -b \"test2\"");
    }

    @Test(expected = UnrecognizedCommandException.class)
    public void testMissingValue() throws UnrecognizedCommandException {
        TestCommand command = new TestCommand();
        CommandInterpreter commandInterpreter = new CommandInterpreter(Utils.newHashSet(command));
        commandInterpreter.parseCommand("test -a test1 -b \"test2\" -c");
    }

    @Test(expected = UnrecognizedCommandException.class)
    public void testValueWithoutParameter() throws UnrecognizedCommandException {
        TestCommand command = new TestCommand();
        CommandInterpreter commandInterpreter = new CommandInterpreter(Utils.newHashSet(command));
        commandInterpreter.parseCommand("test -a test1 -b \"test2\" \"test1\"");
    }

    @Test(expected = IllegalStateException.class)
    public void testOptionalParameter() throws Exception {
        TestCommand command = new TestCommand();
        CommandInterpreter commandInterpreter = new CommandInterpreter(Utils.newHashSet(command));
        ParsedCommand parsedCommand = commandInterpreter.parseCommand("test -b \"test2\" -c \"test 3\"");

        Assert.assertFalse(parsedCommand.getOptional("a").isPresent());
        Assert.assertEquals(parsedCommand.get("b"), "test2");
        Assert.assertEquals(parsedCommand.get("c"), "test 3");

        parsedCommand.get("a"); // Should throw IllegalStateException
    }
}
