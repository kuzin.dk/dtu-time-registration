package com.dema.timeregistration.picocontainer;

import cucumber.api.java.ObjectFactory;
import cucumber.runtime.Utils;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.PicoBuilder;
import org.picocontainer.injectors.Provider;
import org.picocontainer.injectors.ProviderAdapter;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Set;

/**
 * This is a modified version of the PicoFactory that cucumber normally uses, that allows the addition of a provider for
 * a dependency. In this case, it is because the JPA repositories need an EntityProvider as a dependency, which the
 * PicoContainer (Cucumber's standard DI library) can't instantiate by itself.
 * <p>
 * There is an open issue for a similar improvement on GitHub here: https://github.com/cucumber/cucumber-jvm/issues/1446
 */
public class InstanceAcceptingPicoFactory implements ObjectFactory {
    private MutablePicoContainer pico;
    private final Set<Class<?>> classes = new HashSet<>();
    private final Set<Provider> providers = new HashSet<>();

    public void start() {
        pico = new PicoBuilder()
                .withCaching()
                .withLifecycle()
                .build();

        for (Class<?> clazz : classes) {
            pico.addComponent(clazz);
        }

        for (Provider provider : providers) {
            pico.addAdapter(new ProviderAdapter(provider));
        }

        pico.start();
    }

    public void stop() {
        pico.stop();
        pico.dispose();
    }

    public boolean addClass(Class<?> clazz) {
        if (Utils.isInstantiable(clazz) && classes.add(clazz)) {
            addConstructorDependencies(clazz);
        }
        return true;
    }

    public <T> T getInstance(Class<T> type) {
        return pico.getComponent(type);
    }

    private void addConstructorDependencies(Class<?> clazz) {
        for (Constructor constructor : clazz.getConstructors()) {
            for (Class paramClazz : constructor.getParameterTypes()) {
                addClass(paramClazz);
            }
        }
    }

    public void addProvider(Provider provider) {
        providers.add(provider);
    }
}