package com.dema.timeregistration.picocontainer;

import com.dema.timeregistration.repository.JpaCompanyRepository;
import com.dema.timeregistration.repository.JpaProjectRepository;
import com.dema.timeregistration.repository.JpaTaskRepository;
import com.dema.timeregistration.repository.JpaUserRepository;

public class CustomPicoFactory extends InstanceAcceptingPicoFactory {
    public CustomPicoFactory() {
        addProvider(new EntityManagerProvider());
        addClass(JpaUserRepository.class);
        addClass(JpaCompanyRepository.class);
        addClass(JpaProjectRepository.class);
        addClass(JpaTaskRepository.class);
    }
}
