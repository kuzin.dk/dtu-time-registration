package com.dema.timeregistration.service;

import com.dema.timeregistration.TestExceptionHandler;
import com.dema.timeregistration.TestUserProvider;
import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.service.models.UpdateCompanyRequest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;

import java.util.Map;

public class AdministratorServiceSteps {
    private final AdministratorService administratorService;
    private final CompanyRepository companyRepository;
    private final TestUserProvider testUserProvider;
    private final TestExceptionHandler testExceptionHandler;

    private int lastCompanyId = -1;

    public AdministratorServiceSteps(AdministratorService administratorService,
                                     CompanyRepository companyRepository,
                                     TestUserProvider testUserProvider,
                                     TestExceptionHandler testExceptionHandler) {
        this.administratorService = administratorService;
        this.companyRepository = companyRepository;
        this.testUserProvider = testUserProvider;
        this.testExceptionHandler = testExceptionHandler;
    }

    @Then("the subject should be an administrator")
    public void subjectShouldBeAdministrator() {
        Assert.assertEquals(GlobalRole.GLOBAL_ADMINISTRATOR, testUserProvider.getSubject().getGlobalRole());
    }

    @When("the actor creates a company with the following")
    public void actorCreatesCompany(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String name = dataMap.get("Name");

        try {
            administratorService.createCompany(name, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("a company should exist with the following")
    public void companyShouldExist(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String name = dataMap.get("Name");

        companyRepository.findByName(name).orElseThrow(IllegalStateException::new);
    }

    @And("a company with the the following")
    public void company(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String name = dataMap.get("Name");

        Company company = Company.create(name);
        companyRepository.save(company);
        lastCompanyId = company.getId();
    }

    @When("the actor updates the company with the following")
    public void actorUpdatesCompany(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String name = dataMap.get("Name");

        UpdateCompanyRequest request = new UpdateCompanyRequest();

        request.setId(lastCompanyId);
        request.setName(name);

        try {
            administratorService.updateCompany(request, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @When("the actor gives the subject the role {string}")
    public void theActorGivesTheSubjectTheRole(String globalRoleString) {
        GlobalRole globalRole = GlobalRole.valueOf(globalRoleString);

        try {
            administratorService.setGlobalRole(testUserProvider.getSubject().getId(), globalRole, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }
}
