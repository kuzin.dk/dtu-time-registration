package com.dema.timeregistration.service;

import com.dema.timeregistration.TestExceptionHandler;
import com.dema.timeregistration.TestUserProvider;
import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.entity.Role;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.models.CreateProjectRequest;
import com.dema.timeregistration.service.models.UpdateProjectRequest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

public class ProjectServiceSteps {
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;
    private final ProjectService projectService;
    private final TestExceptionHandler testExceptionHandler;
    private final TestUserProvider testUserProvider;

    private int lastProjectId = -1;

    public ProjectServiceSteps(CompanyRepository companyRepository, UserRepository userRepository, ProjectRepository projectRepository, ProjectService projectService, TestExceptionHandler testExceptionHandler, TestUserProvider testUserProvider) {
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.projectService = projectService;
        this.testExceptionHandler = testExceptionHandler;
        this.testUserProvider = testUserProvider;
    }


    @When("the actor creates a project with the following")
    public void actorCreatesProject(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String companyName = dataMap.get("Company");
        String name = dataMap.get("Name");
        String projectLeaderEmail = dataMap.get("ProjectLeader");

        Company company = companyRepository.findByName(companyName).orElseThrow(IllegalStateException::new);
        User projectLeader = userRepository.findByEmail(projectLeaderEmail).orElseThrow(IllegalStateException::new);

        CreateProjectRequest createProjectRequest = new CreateProjectRequest();
        createProjectRequest.setCompanyId(company.getId());
        createProjectRequest.setName(name);

        Date dueDate = Date.from(Instant.now().plusSeconds(60 * 60 * 24 * 14));

        createProjectRequest.setDueDate(Utils.getDateFormat().format(dueDate));
        createProjectRequest.setProjectLeaderId(projectLeader.getId());

        try {
            projectService.createProject(createProjectRequest, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @And("a project with the following")
    public void project(DataTable info) throws ParseException {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String companyName = dataMap.get("Company");
        String name = dataMap.get("Name");
        String projectLeaderEmail = dataMap.get("ProjectLeader");
        Instant dueDate;

        if (dataMap.get("Due Date") != null) {
            dueDate = Utils.getDateFormat().parse(dataMap.get("Due Date")).toInstant();
        } else {
            dueDate = Instant.now().plusSeconds(60 * 60 * 24);
        }

        Company company = companyRepository.findByName(companyName).orElseThrow(IllegalStateException::new);

        User projectLeader;

        if (projectLeaderEmail.equals("{{actor}}")) {
            projectLeader = testUserProvider.getActor().getUser();
        } else if (projectLeaderEmail.equals("{{subject}}")) {
            projectLeader = testUserProvider.getSubject();
        } else {
            projectLeader = userRepository.findByEmail(projectLeaderEmail).orElseThrow(IllegalStateException::new);
        }

        Project project = Project.create(name, company, dueDate, projectLeader);

        projectRepository.save(project);
        lastProjectId = project.getId();
    }

    @When("the actor removes the project role from the subject for project {string} for company {string}")
    public void removeProjectRole(String project, String company) {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        try {
            projectService.removeProjectRole(testUserProvider.getSubject().getId(), foundProject.getId(), testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("the subject should not have a project role for project {string} for company {string}")
    public void invertCheckRole(String project, String company) {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);
        Assert.assertFalse(testUserProvider.getSubject().getProjectRoles()
                .stream()
                .anyMatch(pr -> pr.getProject().equals(foundProject)));
    }

    @Then("a project should exist with the following")
    public void projectShouldExist(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String companyName = dataMap.get("Company");
        String name = dataMap.get("Name");
        String projectLeaderEmail = dataMap.get("ProjectLeader");

        Company company = companyRepository.findByName(companyName).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, name).orElseThrow(IllegalStateException::new);

        User user = userRepository.findByEmail(projectLeaderEmail).orElseThrow(IllegalStateException::new);
        Assert.assertTrue(user.getProjectRoles().stream().anyMatch(pr -> pr.getProject().equals(project)));
    }

    @When("the actor updates the project with the following")
    public void actorUpdateProject(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);
        String name = dataMap.get("Name");

        Project project = projectRepository.findById(lastProjectId).orElseThrow(IllegalStateException::new);

        UpdateProjectRequest updateProjectRequest = new UpdateProjectRequest();


        updateProjectRequest.setId(project.getId());
        updateProjectRequest.setName(name);

        if (dataMap.get("Due Date") != null) {
            updateProjectRequest.setDueDate(dataMap.get("Due Date"));
        }

        try {
            projectService.updateProject(updateProjectRequest, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @When("the actor assign the role {string} to the subject for project {string} for company {string}")
    public void assignRoleToUser(String roleString, String project, String company) throws IllegalStateException {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        try {
            projectService.assignProjectRole(testUserProvider.getSubject().getId(), foundProject.getId(), Role.valueOf(roleString), testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("the subject's role should be {string} for project {string} for company {string}")
    public void checkRole(String role, String project, String company) {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        Assert.assertTrue(testUserProvider.getSubject().getProjectRoles()
                .stream()
                .anyMatch(pr -> pr.getProject().equals(foundProject) && pr.getRole().equals(Role.valueOf(role))));
    }
}
