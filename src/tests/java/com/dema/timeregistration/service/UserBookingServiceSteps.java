package com.dema.timeregistration.service;

import com.dema.timeregistration.TestExceptionHandler;
import com.dema.timeregistration.TestUserProvider;
import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.models.BookUserRequest;
import com.dema.timeregistration.service.models.RegisterUserAbsenceRequest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;

import java.text.ParseException;
import java.time.Instant;
import java.util.Map;


public class UserBookingServiceSteps {
    private final UserBookingService userBookingService;
    private final UserRepository userRepository;
    private final ProjectRepository projectRepository;
    private final CompanyRepository companyRepository;
    private final TestUserProvider testUserProvider;
    private final TestExceptionHandler testExceptionHandler;

    public UserBookingServiceSteps(UserBookingService userBookingService,
                                   UserRepository userRepository,
                                   ProjectRepository projectRepository,
                                   CompanyRepository companyRepository,
                                   TestUserProvider testUserProvider,
                                   TestExceptionHandler testExceptionHandler) {
        this.userBookingService = userBookingService;
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.companyRepository = companyRepository;
        this.testUserProvider = testUserProvider;
        this.testExceptionHandler = testExceptionHandler;
    }

    @When("the actor registers an absence with the following")
    public void actorCreatesBooking(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        RegisterUserAbsenceRequest request = new RegisterUserAbsenceRequest();

        request.setStartTime(dataMap.get("Start Time"));
        request.setEndTime(dataMap.get("End Time"));
        request.setReason(dataMap.get("Reason"));


        try {
            userBookingService.registerUserAbsence(request, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("the actor should have an absence with the following")
    public void shouldHaveAbsence(DataTable info) throws ParseException {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        User user = testUserProvider.getActor().getUser();
        boolean exists = false;

        for (UserAbsence userAbsence : user.getUserAbsences()) {
            if (userAbsence.getReason().equals(dataMap.get("Reason"))
                    && userAbsence.getStartTime().equals(Utils.getDateFormat().parse(dataMap.get("Start Time")).toInstant())
                    && userAbsence.getEndTime().equals(Utils.getDateFormat().parse(dataMap.get("End Time")).toInstant())) {
                exists = true;
            }
        }

        Assert.assertTrue(exists);
    }

    @And("the subject has an absence with the following")
    public void theActorHasAnAbsenceWithTheFollowing(DataTable info) throws ParseException {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        User user = userRepository.findById(testUserProvider.getSubject().getId())
                .orElseThrow(IllegalStateException::new);

        Instant start = Utils.getDateFormat().parse(dataMap.get("Start Time")).toInstant();
        Instant end = Utils.getDateFormat().parse(dataMap.get("End Time")).toInstant();

        UserAbsence userAbsence = UserAbsence.create(user, start, end, dataMap.get("Reason"));
        user.getUserAbsences().add(userAbsence);
        userRepository.save(user);
    }

    @When("the actor books the subject to the project {string} for company {string} from {string} to {string}")
    public void theActorBooksTheSubjectToTheProjectFromTo(String project, String company, String startTime, String endTime) {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        BookUserRequest request = new BookUserRequest();
        request.setUserId(testUserProvider.getSubject().getId());
        request.setProjectId(foundProject.getId());
        request.setStartTime(startTime);
        request.setEndTime(endTime);

        try {
            userBookingService.bookUser(request, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("the subject should have a booking to the project {string} for company {string} from {string} to {string}")
    public void theSubjectShouldHaveABookingToTheProjectForCompanyFromTo(String project, String company, String startTime, String endTime) throws ParseException {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        Instant start = Utils.getDateFormat().parse(startTime).toInstant();
        Instant end = Utils.getDateFormat().parse(endTime).toInstant();

        User user = userRepository.findById(testUserProvider.getSubject().getId()).orElseThrow(IllegalStateException::new);
        Assert.assertTrue(user.getUserProjectBookings()
                .stream()
                .anyMatch(pb -> pb.getProject().equals(foundProject) &&
                        pb.getStartTime().equals(start) &&
                        pb.getEndTime().equals(end)));
    }

    @And("the subject has a booking to the project {string} for company {string} from {string} to {string}")
    public void theSubjectHasABookingToTheProjectForCompanyFromTo(String project, String company, String startTime, String endTime) throws ParseException {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        Instant start = Utils.getDateFormat().parse(startTime).toInstant();
        Instant end = Utils.getDateFormat().parse(endTime).toInstant();


        User user = userRepository.findById(testUserProvider.getSubject().getId()).orElseThrow(IllegalStateException::new);

        UserProjectBooking userProjectBooking = UserProjectBooking.create(user, foundProject, start, end);
        user.getUserProjectBookings().add(userProjectBooking);
        userRepository.save(user);
    }
}
