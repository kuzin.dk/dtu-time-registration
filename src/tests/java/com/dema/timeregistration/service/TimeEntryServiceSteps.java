package com.dema.timeregistration.service;

import com.dema.timeregistration.TestExceptionHandler;
import com.dema.timeregistration.TestUserProvider;
import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.TaskRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.models.RegisterTimeEntryRequest;
import com.dema.timeregistration.service.models.UpdateTimeEntryRequest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;

import java.text.ParseException;
import java.time.Instant;
import java.util.Map;

public class TimeEntryServiceSteps {
    private final TimeEntryService timeEntryService;
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final TestUserProvider testUserProvider;
    private final TestExceptionHandler testExceptionHandler;


    public TimeEntryServiceSteps(TimeEntryService timeEntryService, TaskRepository taskRepository, ProjectRepository projectRepository, CompanyRepository companyRepository, UserRepository userRepository, TestUserProvider testUserProvider, TestExceptionHandler testExceptionHandler) {
        this.timeEntryService = timeEntryService;
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.testUserProvider = testUserProvider;
        this.testExceptionHandler = testExceptionHandler;
    }

    @When("the actor updates the time entry of a task {string}")
    public void actorUpdatesTimeEntry(String task) {

    }

    @When("the actor registers a time entry with the following")
    public void registerTimeEntry(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findByProjectAndName(project, dataMap.get("Task")).orElseThrow(IllegalStateException::new);

        RegisterTimeEntryRequest request = new RegisterTimeEntryRequest();

        request.setTaskId(task.getId());
        request.setStartTime(dataMap.get("Start Time"));
        request.setEndTime(dataMap.get("End Time"));

        try {
            timeEntryService.registerTimeEntry(request, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("the subject should have a time entry with the following")
    public void timeEntryShouldExist(DataTable info) throws ParseException {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findByProjectAndName(project, dataMap.get("Task")).orElseThrow(IllegalStateException::new);

        Instant startTime = Utils.getDateFormat().parse(dataMap.get("Start Time")).toInstant();

        UserProjectBooking userProjectBooking = testUserProvider.getSubject().getUserProjectBookings()
                .stream()
                .filter(pb -> pb.getProject().equals(project))
                .findFirst().orElseThrow(IllegalStateException::new);

        TimeEntry timeEntry = userProjectBooking.getTimeEntries().stream()
                .filter(te -> te.getTask().equals(task) && te.getStartTime().equals(startTime))
                .findFirst()
                .orElseThrow(IllegalStateException::new);

        Assert.assertEquals(timeEntry.getEndTime(), Utils.getDateFormat().parse(dataMap.get("End Time")).toInstant());
    }

    @And("the subject has a time entry with the following")
    public void theSubjectHasATimeEntryWithTheFollowing(DataTable info) throws ParseException {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findByProjectAndName(project, dataMap.get("Task")).orElseThrow(IllegalStateException::new);

        UserProjectBooking userProjectBooking = testUserProvider.getSubject().getUserProjectBookings()
                .stream()
                .filter(pb -> pb.getProject().equals(project))
                .findFirst().orElseThrow(IllegalStateException::new);

        Instant startTime = Utils.getDateFormat().parse(dataMap.get("Start Time")).toInstant();
        Instant endTime = Utils.getDateFormat().parse(dataMap.get("End Time")).toInstant();

        TimeEntry timeEntry = TimeEntry.create(task, userProjectBooking, startTime, endTime);
        userProjectBooking.getTimeEntries().add(timeEntry);
        userRepository.save(testUserProvider.getSubject());
    }

    @When("the actor updates a time entry with the following")
    public void theActorUpdatesATimeEntryWithTheFollowing(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findByProjectAndName(project, dataMap.get("Task")).orElseThrow(IllegalStateException::new);

        UpdateTimeEntryRequest request = new UpdateTimeEntryRequest();

        request.setTaskId(task.getId());
        request.setOriginalStartTime(dataMap.get("Original Start Time"));
        request.setStartTime(dataMap.get("Start Time"));
        request.setEndTime(dataMap.get("End Time"));

        try {
            timeEntryService.updateTimeEntry(request, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }
}
