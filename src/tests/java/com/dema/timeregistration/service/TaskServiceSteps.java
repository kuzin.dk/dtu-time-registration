package com.dema.timeregistration.service;

import com.dema.timeregistration.TestExceptionHandler;
import com.dema.timeregistration.TestUserProvider;
import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.entity.Task;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.TaskRepository;
import com.dema.timeregistration.service.models.CreateTaskRequest;
import com.dema.timeregistration.service.models.UpdateTaskRequest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;

import java.time.Duration;
import java.util.Map;

public class TaskServiceSteps {
    private final TaskService taskService;
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final CompanyRepository companyRepository;
    private final TestUserProvider testUserProvider;
    private final TestExceptionHandler testExceptionHandler;

    public TaskServiceSteps(TaskService taskService, TaskRepository taskRepository, ProjectRepository projectRepository, CompanyRepository companyRepository, TestUserProvider testUserProvider, TestExceptionHandler testExceptionHandler) {
        this.taskService = taskService;
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.companyRepository = companyRepository;
        this.testUserProvider = testUserProvider;
        this.testExceptionHandler = testExceptionHandler;
    }

    @When("the actor creates a task with the following")
    public void actorCreatesTask(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);

        CreateTaskRequest request = new CreateTaskRequest();

        request.setName(dataMap.get("Name"));
        request.setProjectId(project.getId());
        request.setEstimate(dataMap.get("Estimate"));
        request.setStatus(dataMap.get("Status"));
        request.setDescription(dataMap.get("Description"));

        try {
            taskService.createTask(request, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }

    @Then("a task should exist with the following")
    public void taskShouldExist(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findByProjectAndName(project, dataMap.get("Name")).orElseThrow(IllegalStateException::new);

        Assert.assertEquals(dataMap.get("Estimate"), task.getEstimate().toString());
        Assert.assertEquals(dataMap.get("Status"), task.getStatus());
        Assert.assertEquals(dataMap.get("Description"), task.getDescription());
    }

    @Given("a task with the following")
    public void aTaskWithTheFollowing(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);

        Task task = Task.create(
                dataMap.get("Name"),
                project,
                dataMap.get("Description"),
                dataMap.get("Status"),
                Duration.parse(dataMap.get("Estimate"))
        );

        taskRepository.save(task);
    }

    @When("the actor updates a task with the following")
    public void actorUpdatesTask(DataTable info) {
        Map<String, String> dataMap = info.asMap(String.class, String.class);

        Company company = companyRepository.findByName(dataMap.get("Company")).orElseThrow(IllegalStateException::new);
        Project project = projectRepository.findByCompanyAndName(company, dataMap.get("Project")).orElseThrow(IllegalStateException::new);
        Task task = taskRepository.findByProjectAndName(project, dataMap.get("OldName")).orElseThrow(IllegalStateException::new);

        UpdateTaskRequest updateTaskRequest = new UpdateTaskRequest();

        updateTaskRequest.setId(task.getId());
        updateTaskRequest.setName(dataMap.get("Name"));
        updateTaskRequest.setDescription(dataMap.get("Description"));
        updateTaskRequest.setStatus(dataMap.get("Status"));
        updateTaskRequest.setEstimate(dataMap.get("Estimate"));

        try {
            taskService.updateTask(updateTaskRequest, testUserProvider.getActor());
        } catch (Exception e) {
            testExceptionHandler.setException(e);
        }
    }
}
