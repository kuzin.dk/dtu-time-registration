Feature: Create Task
  Description: The Project Leader (or administrator) creates a new task
  Actors: Project Leader, Global Administrator

  Scenario: Create Task Successfully
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Inside          |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Inside" for company "DTU"
    When the actor creates a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    Then a task should exist with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |

  Scenario: Non Project Leader Attempts to Create Task
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Inside          |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a user
    When the actor creates a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Create Task with taken name
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Inside          |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Inside" for company "DTU"
    And a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    When the actor creates a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    Then an exception of type "EntityAlreadyExistsException" should be thrown