Feature: Update Task
  Description: The Project Leader (or administrator) updates an existing task
  Actors: Project Leader, Global Administrator

  Scenario: Create Task Successfully
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Inside          |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Inside" for company "DTU"
    And a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    When the actor updates a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | OldName     | Authentication System |
      | Name        | Login System          |
      | Description | Login System          |
      | Status      | Started               |
      | Estimate    | PT15H                 |
    Then a task should exist with the following
      | Company     | DTU          |
      | Project     | Inside       |
      | Name        | Login System |
      | Description | Login System |
      | Status      | Started      |
      | Estimate    | PT15H        |

  Scenario: Non Project Leader Attempts to Update Task
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Inside          |
      | ProjectLeader | mads@kahler.biz |
    And a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    And the actor is a user
    When the actor updates a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | OldName     | Authentication System |
      | Name        | Login System          |
      | Description | Login System          |
      | Status      | Started               |
      | Estimate    | PT15H                 |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Update Task to taken name
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Inside          |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Inside" for company "DTU"
    And a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    And a task with the following
      | Company     | DTU          |
      | Project     | Inside       |
      | Name        | Login System |
      | Description | Login System |
      | Status      | Not Started  |
      | Estimate    | PT10H        |
    When the actor updates a task with the following
      | Company     | DTU                   |
      | Project     | Inside                |
      | OldName     | Authentication System |
      | Name        | Login System          |
      | Description | Login System          |
      | Status      | Started               |
      | Estimate    | PT15H                 |
    Then an exception of type "EntityAlreadyExistsException" should be thrown