Feature: Login User
  Description: An unregistered user logs in with their user
  Actors: Unregistered User (Anyone)

  Scenario: Log in Successfully
    Given a user with the email "mads@kahler.biz"
    When a user logs in with the email "mads@kahler.biz"
    Then a user context with a user with the email "mads@kahler.biz" should be received

  Scenario: Attempt to log in with wrong email
    When a user logs in with the email "mads@kahler.biz"
    Then an exception of type "EntityNotFoundException" should be thrown