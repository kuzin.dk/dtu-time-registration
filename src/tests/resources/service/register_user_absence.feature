Feature: Register user absence
  Description: An unregistered user logs in with their user
  Actors: Unregistered User (Anyone)

  Scenario: User registers absence successfully
    Given the actor is a user
    When the actor registers an absence with the following
      | Start Time | 10/05/2019 12:50 |
      | End Time   | 10/05/2019 12:55 |
      | Reason     | Sick             |
    Then the actor should have an absence with the following
      | Start Time | 10/05/2019 12:50 |
      | End Time   | 10/05/2019 12:55 |
      | Reason     | Sick             |

  Scenario: User double books absence
    Given the actor is a user
    And the subject is the actor
    And the subject has an absence with the following
      | Start Time | 10/05/2019 12:50 |
      | End Time   | 10/05/2019 12:55 |
      | Reason     | Sick             |
    When the actor registers an absence with the following
      | Start Time | 10/05/2019 12:50 |
      | End Time   | 10/05/2019 12:55 |
      | Reason     | Sick             |
    Then an exception of type "EntityAlreadyExistsException" should be thrown


  Scenario: User attempts to book absence already taken by project booking
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    Given the actor is a user
    And the subject is the actor
    And the subject has a booking to the project "Test Project" for company "DTU" from "10/05/2019 12:00" to "10/05/2019 16:00"
    When the actor registers an absence with the following
      | Start Time | 10/05/2019 12:50 |
      | End Time   | 10/05/2019 12:55 |
      | Reason     | Sick             |
    Then an exception of type "EntityAlreadyExistsException" should be thrown