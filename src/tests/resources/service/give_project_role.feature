Feature: Give project role to a user
  Description: A user who has the role of global administrator or project role change a users role to project leader
  Actors: A user who has the global role as administrator or/and role as project leader

  Scenario: Assign the role project leader to a user as a project leader
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Test Project" for company "DTU"
    And the subject is a user
    When the actor assign the role "PROJECT_LEADER" to the subject for project "Test Project" for company "DTU"
    Then the subject's role should be "PROJECT_LEADER" for project "Test Project" for company "DTU"

  Scenario: Upgrade the role project leader to a user as a project leader
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Test Project" for company "DTU"
    And the subject is a user with role "SR_DEVELOPER" for project "Test Project" for company "DTU"
    When the actor assign the role "PROJECT_LEADER" to the subject for project "Test Project" for company "DTU"
    Then the subject's role should be "PROJECT_LEADER" for project "Test Project" for company "DTU"

  Scenario: Assign the role project leader to a user as a global administrator
    Given the actor is an administrator
    And a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the subject is a user
    When the actor assign the role "PROJECT_LEADER" to the subject for project "Test Project" for company "DTU"
    Then the subject's role should be "PROJECT_LEADER" for project "Test Project" for company "DTU"

  Scenario: A non-project leader and non-administrator user attempts to assign the role project leader to a user
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a user
    And the subject is a user
    When the actor assign the role "PROJECT_LEADER" to the subject for project "Test Project" for company "DTU"
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: A user with the role project leader removes a role from a user
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Test Project" for company "DTU"
    And the subject is a user with role "SR_DEVELOPER" for project "Test Project" for company "DTU"
    When the actor removes the project role from the subject for project "Test Project" for company "DTU"
    Then the subject should not have a project role for project "Test Project" for company "DTU"

  Scenario: A user with the global role administrator removes a role from a user
    Given the actor is an administrator
    And a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the subject is a user with role "SR_DEVELOPER" for project "Test Project" for company "DTU"
    When the actor removes the project role from the subject for project "Test Project" for company "DTU"
    Then the subject should not have a project role for project "Test Project" for company "DTU"

  Scenario: A non-project leader and non-administrator user attempts to remove a role from a user
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a user
    And the subject is a user with role "SR_DEVELOPER" for project "Test Project" for company "DTU"
    When the actor removes the project role from the subject for project "Test Project" for company "DTU"
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: A project leader attempts to remove him- or herself from a project as the last project leader for the project
    Given a company with the the following
      | Name | DTU |
    And the actor is a user
    And a project with the following
      | Company       | DTU          |
      | Name          | Test Project |
      | ProjectLeader | {{actor}}    |
    And the subject is the actor
    When the actor removes the project role from the subject for project "Test Project" for company "DTU"
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: An administrator attempts to remove the only project leader from a project
    Given the actor is an administrator
    And a company with the the following
      | Name | DTU |
    And the subject is a user
    And a project with the following
      | Company       | DTU          |
      | Name          | Test Project |
      | ProjectLeader | {{subject}}  |
    When the actor removes the project role from the subject for project "Test Project" for company "DTU"
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: An administrator attempts to downgrade the only project leader from a project
    Given the actor is an administrator
    And a company with the the following
      | Name | DTU |
    And the subject is a user
    And a project with the following
      | Company       | DTU          |
      | Name          | Test Project |
      | ProjectLeader | {{subject}}  |
    When the actor assign the role "SR_DEVELOPER" to the subject for project "Test Project" for company "DTU"
    Then an exception of type "AccessDeniedException" should be thrown
