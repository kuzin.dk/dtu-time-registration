Feature: Update Project
  Description: The Administrator updates a project
  Actors: Administrator

  Scenario: Update Project Successfully
    Given the actor is an administrator
    And a user with the email "mads@kahler.biz"
    And a company with the the following
      | Name | DTU |
    And a project with the following
      | Company       | DTU              |
      | Name          | Old Name         |
      | ProjectLeader | mads@kahler.biz  |
      | Due Date      | 05/05/2019 12:00 |
    When the actor updates the project with the following
      | Name     | New Name         |
      | Due Date | 10/05/2019 12:00 |
    Then a project should exist with the following
      | Company       | DTU              |
      | Name          | New Name         |
      | ProjectLeader | mads@kahler.biz  |
      | Due Date      | 10/05/2019 12:00 |

  Scenario: Non Administrator Attempts to Update project
    Given the actor is a user
    And a user with the email "mads@kahler.biz"
    And a company with the the following
      | Name | DTU |
    And a project with the following
      | Company       | DTU             |
      | Name          | Old Name        |
      | ProjectLeader | mads@kahler.biz |
    When the actor updates the project with the following
      | Name | New Name |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Update Company to taken name
    Given the actor is an administrator
    And a user with the email "mads@kahler.biz"
    And a company with the the following
      | Name | DTU |
    And a project with the following
      | Company       | DTU             |
      | Name          | New Name        |
      | ProjectLeader | mads@kahler.biz |
    And a project with the following
      | Company       | DTU             |
      | Name          | Old Name        |
      | ProjectLeader | mads@kahler.biz |
    When the actor updates the project with the following
      | Name | New Name |
    Then an exception of type "EntityAlreadyExistsException" should be thrown