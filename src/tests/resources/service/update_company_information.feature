Feature: Update Company
  Description: The Administrator updates a company's information
  Actors: Administrator

  Scenario: Update Company Successfully
    Given the actor is an administrator
    And a company with the the following
      | Name | Old Name |
    When the actor updates the company with the following
      | Name | New Name |
    Then a company should exist with the following
      | Name | New Name |

  Scenario: Non Administrator Attempts to Update Company
    Given the actor is a user
    When the actor updates the company with the following
      | Name | DTU |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Update Company to taken name
    Given the actor is an administrator
    And a company with the the following
      | Name | DTU |
    And a company with the the following
      | Name | Old Name |
    When the actor updates the company with the following
      | Name | DTU |
    Then an exception of type "EntityAlreadyExistsException" should be thrown