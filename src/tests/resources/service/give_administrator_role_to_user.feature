Feature: Give Administrator Role To User
  Description: The Administrator Role is given to a user so that they can perform administrative actions on the app
  Actors: Administrator

  Scenario: Give User Administrator Role Successfully
    Given the actor is an administrator
    And the subject is a user
    When the actor gives the subject the role "GLOBAL_ADMINISTRATOR"
    Then the subject should be an administrator

  Scenario: Non Administrator Attempts to Give Administrator Role to User
    Given the actor is a user
    And the subject is a user
    When the actor gives the subject the role "GLOBAL_ADMINISTRATOR"
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Only Administrator Attempts to Remove Himself From Role
    Given the actor is an administrator
    And the subject is the actor
    When the actor gives the subject the role "GLOBAL_USER"
    Then an exception of type "AccessDeniedException" should be thrown