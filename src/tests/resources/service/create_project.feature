Feature: Create Project
  Description: The Administrator creates a new project
  Actors: Administrator

  Scenario: Create Project Successfully
    Given the actor is an administrator
    And a user with the email "mads@kahler.biz"
    And a company with the the following
      | Name | DTU |
    When the actor creates a project with the following
      | Company       | DTU              |
      | Name          | DTU Inside       |
      | ProjectLeader | mads@kahler.biz  |
      | Due Date      | 05/05/2019 12:00 |
    Then a project should exist with the following
      | Company       | DTU              |
      | Name          | DTU Inside       |
      | ProjectLeader | mads@kahler.biz  |
      | Due Date      | 05/05/2019 12:00 |

  Scenario: Non Administrator Attempts to create project
    Given the actor is a user
    And a user with the email "mads@kahler.biz"
    And a company with the the following
      | Name | DTU |
    When the actor creates a project with the following
      | Company       | DTU             |
      | Name          | DTU Inside      |
      | ProjectLeader | mads@kahler.biz |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Create Project With Taken Name
    Given the actor is an administrator
    And a user with the email "mads@kahler.biz"
    And a company with the the following
      | Name | DTU |
    And a project with the following
      | Company       | DTU             |
      | Name          | DTU Inside      |
      | ProjectLeader | mads@kahler.biz |
    When the actor creates a project with the following
      | Company       | DTU             |
      | Name          | DTU Inside      |
      | ProjectLeader | mads@kahler.biz |
    Then an exception of type "EntityAlreadyExistsException" should be thrown
