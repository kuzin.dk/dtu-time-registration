Feature: Register User
  Description: An unregistered user registers their user
  Actors: Unregistered User (Anyone)

  Scenario: Register Successfully
    When a user registers an account with the email address "mads@kahler.biz" and the name "Mads Kahler"
    Then a user should exist with the email address "mads@kahler.biz" and the name "Mads Kahler"

  Scenario: Attempt to register with taken email
    Given a user with the email "mads@kahler.biz"
    When a user registers an account with the email address "mads@kahler.biz" and the name "Mads Kahler"
    Then an exception of type "EntityAlreadyExistsException" should be thrown