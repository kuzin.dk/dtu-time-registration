Feature: Update Time Entry
  Description: A user updates the time that they have spend on a given task
  Actors: User with Booking in relevant project

  Scenario: A user that is booked in a project tries to update a time entry for a task
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And a task with the following
      | Company     | DTU                   |
      | Project     | Test Project          |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    And the actor is a user with a booking for project "Test Project" for company "DTU" between the dates "01/05/2019 00:00" and "03/05/2019 00:00"
    And the subject is the actor
    And the subject has a time entry with the following
      | Company    | DTU                   |
      | Project    | Test Project          |
      | Task       | Authentication System |
      | Start Time | 01/05/2019 08:00      |
      | End Time   | 01/05/2019 12:00      |
    When the actor updates a time entry with the following
      | Company             | DTU                   |
      | Project             | Test Project          |
      | Task                | Authentication System |
      | Original Start Time | 01/05/2019 08:00      |
      | Start Time          | 02/05/2019 08:00      |
      | End Time            | 02/05/2019 12:00      |
    Then the subject should have a time entry with the following
      | Company    | DTU                   |
      | Project    | Test Project          |
      | Task       | Authentication System |
      | Start Time | 02/05/2019 08:00      |
      | End Time   | 02/05/2019 12:00      |

  Scenario: A user that is booked in a project tries to update a time entry, but is not booked to the project at the right time
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And a task with the following
      | Company     | DTU                   |
      | Project     | Test Project          |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    And the actor is a user with a booking for project "Test Project" for company "DTU" between the dates "01/05/2019 00:00" and "03/05/2019 00:00"
    And the subject is the actor
    And the subject has a time entry with the following
      | Company    | DTU                   |
      | Project    | Test Project          |
      | Task       | Authentication System |
      | Start Time | 01/05/2019 08:00      |
      | End Time   | 01/05/2019 12:00      |
    When the actor updates a time entry with the following
      | Company             | DTU                   |
      | Project             | Test Project          |
      | Task                | Authentication System |
      | Original Start Time | 01/05/2019 08:00      |
      | Start Time          | 05/05/2019 08:00      |
      | End Time            | 05/05/2019 12:00      |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: A user that is booked in a project tries to update a time entry, but a time entry in the given interval already exists
    Given a company with the the following
      | Name | DTU |
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And a task with the following
      | Company     | DTU                   |
      | Project     | Test Project          |
      | Name        | Authentication System |
      | Description | Authentication System |
      | Status      | Not Started           |
      | Estimate    | PT10H                 |
    And the actor is a user with a booking for project "Test Project" for company "DTU" between the dates "01/05/2019 00:00" and "03/05/2019 00:00"
    And the subject is the actor
    And the subject has a time entry with the following
      | Company    | DTU                   |
      | Project    | Test Project          |
      | Task       | Authentication System |
      | Start Time | 02/05/2019 08:00      |
      | End Time   | 02/05/2019 12:00      |
    And the subject has a time entry with the following
      | Company    | DTU                   |
      | Project    | Test Project          |
      | Task       | Authentication System |
      | Start Time | 01/05/2019 08:00      |
      | End Time   | 01/05/2019 12:00      |
    When the actor updates a time entry with the following
      | Company             | DTU                   |
      | Project             | Test Project          |
      | Task                | Authentication System |
      | Original Start Time | 01/05/2019 08:00      |
      | Start Time          | 02/05/2019 08:00      |
      | End Time            | 02/05/2019 12:00      |
    Then an exception of type "EntityAlreadyExistsException" should be thrown