Feature: Create Company
  Description: The Administrator creates a new company that the company can deliver to
  Actors: Administrator

  Scenario: Create Company Successfully
    Given the actor is an administrator
    When the actor creates a company with the following
      | Name | DTU |
    Then a company should exist with the following
      | Name | DTU |

  Scenario: Non Administrator Attempts to create company
    Given the actor is a user
    When the actor creates a company with the following
      | Name | DTU |
    Then an exception of type "AccessDeniedException" should be thrown

  Scenario: Create Company with taken name
    Given the actor is an administrator
    And a company with the the following
      | Name | DTU |
    When the actor creates a company with the following
      | Name | DTU |
    Then an exception of type "EntityAlreadyExistsException" should be thrown